export default (notes, id) => {
  const personNotes = notes.filter((note) => {
    return note.personId === id
  })

  return personNotes
}
