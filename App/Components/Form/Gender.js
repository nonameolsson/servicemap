// @flow
import React from 'react'
import { Text, View } from 'native-base'
import { Field } from 'redux-form'

// Custom components
import RadioButton from './RadioButton'

// Styles
import styles from './Styles/GenderStyle'

// Types
type State = {
  selected: string
}

export default class Gender extends React.Component<void, State> {
  state = {
    selected: 'male'
  }

  onPress (selected: string) {
    this.setState({
      selected
    })
  }

  render () {
    return (
      <View>
        <Text style={styles.label}>Gender</Text>
        <Field
          changeState={() => this.onPress('male')}
          component={RadioButton}
          icon='man'
          label='Male'
          name={'gender'}
          selected={this.state.selected}
          val='male'
        />
        <Field
          changeState={() => this.onPress('female')}
          component={RadioButton}
          icon='woman'
          label='Female'
          name={'gender'}
          selected={this.state.selected}
          val='female'
        />
      </View>
    )
  }
}
