// @flow
import React from 'react'
import { TabNavigator } from 'react-navigation'
import { NavigationComponent } from 'react-native-material-bottom-navigation'
import { Icon } from 'native-base'

// Screens
import VisitInfoScreen from '../Containers/VisitInfoScreen'
import VisitHistoryScreen from '../Containers/VisitHistoryScreen'

// Custom styles
import styles from './Styles/NavigationStyles'
import Colors from '../Themes/Colors'

/* -----------
  Main tab navigation
  Map and Visit list
  ----------- */
const visitTabsRoutes = {
  VisitHistoryTab: {
    screen: VisitHistoryScreen,
    navigationOptions: {
      tabBarLabel: 'History'
    }
  },
  VisitInfoTab: {
    screen: VisitInfoScreen,
    navigationOptions: {
      tabBarLabel: 'Info'
    }
  }
}

const visitTabsConfig = {
  tabBarComponent: NavigationComponent,
  tabBarPosition: 'bottom',
  animationEnabled: true,
  backBehavior: 'none',
  tabBarOptions: {
    bottomNavigationOptions: {
      style: { borderTopWidth: 0, elevation: 8 },
      activeLabelColor: Colors.activeLabelColor,
      backgroundColor: Colors.backgroundColor,
      labelColor: Colors.labelColor,
      rippleColor: Colors.primary,
      tabs: {
        VisitHistoryTab: {
          activeIcon: (
            <Icon name='list' style={styles.bottomNavigationActiveIcon} />
          ),
          icon: <Icon name='list' style={styles.bottomNavigationIcon} />
        },
        VisitInfoTab: {
          activeIcon: (
            <Icon name='information-circle' style={styles.bottomNavigationActiveIcon} />
          ),
          icon: <Icon name='information-circle' style={styles.bottomNavigationIcon} />
        }
      }
    }
  }
}

export default TabNavigator(visitTabsRoutes, visitTabsConfig)
