// @flow
import React from 'react'
import { H2, View } from 'native-base'
import { Grid, Col, Row } from 'react-native-easy-grid'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'

// Custom components
import { TextInput, TextBox } from '../../Form'
import { required, number } from '../../Form/Validation'
import Gender from '../../Form/Gender'

class EditPersonForm extends React.Component<{}, {}> {
  render () {
    return (
      <View style={{flex: 1}}>
        <Grid>
          <H2>Details about person</H2>
          <Row>
            <Col size={3}>
              <Field
                name={'name'}
                component={TextInput}
                label='Name'
                validate={[required]}
              />
            </Col>
            <Col size={1}>
              <Field
                name={'age'}
                component={TextInput}
                label='Age'
                keyboardType='numeric'
                validate={[required, number]}
              />
            </Col>
          </Row>
        </Grid>
        <Row>
          <Col>
            <Gender />
          </Col>
        </Row>
        <Row>
          <Col>
            <Field name={'language'} component={TextInput} label='Language' />
          </Col>
        </Row>
        <Row>
          <Col>
            <Field name={'comments'} component={TextBox} label='Comments' />
          </Col>
        </Row>
      </View>
    )
  }
}

let FormComponent = EditPersonForm

// Decorate with redux-form
FormComponent = reduxForm({
  form: 'editPerson'
})(EditPersonForm)

FormComponent = connect(
  (state, props) => {
    return {
      initialValues: {
        ...props.person
      }
    }
  }
)(FormComponent)

export default FormComponent
