// @flow
import React from 'react'
import { Content, H2 } from 'native-base'
import { Grid, Col, Row } from 'react-native-easy-grid'
import { Field } from 'redux-form'

// Custom components
import { TextInput, TextBox } from '../../Form'
import { required, number } from '../../Form/Validation'
import Gender from '../../Form/Gender'

// Styles
// import styles from './Styles/Page1Style'

export default class Page1 extends React.Component {
  componentDidMount () {
    this.props.navigation.setParams({
      title: 'Add new person',
      subtitle: 'Details'
    })
  }

  render () {
    return (
      <Content padder>
        <Grid>
          <H2>Details about person</H2>
          <Row>
            <Col size={3}>
              <Field
                name={'name'}
                component={TextInput}
                label='Name'
                validate={[required]}
              />
            </Col>
            <Col size={1}>
              <Field
                name={'age'}
                component={TextInput}
                label='Age'
                keyboardType='numeric'
                validate={[required, number]}
              />
            </Col>
          </Row>
        </Grid>
        <Row>
          <Col>
            <Gender />
          </Col>
        </Row>
        <Row>
          <Col>
            <Field name={'language'} component={TextInput} label='Language' />
          </Col>
        </Row>
        <Row>
          <Col>
            <Field name={'comments'} component={TextBox} label='Comments' />
          </Col>
        </Row>
      </Content>
    )
  }
}
