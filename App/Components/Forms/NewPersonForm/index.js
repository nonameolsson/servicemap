import NewPersonForm from './NewPersonForm'
import Page1 from './Page1'
import Page2 from './Page2'
import Page3 from './Page3'
import Page4 from './Page4'

export { NewPersonForm, Page1, Page2, Page3, Page4 }
