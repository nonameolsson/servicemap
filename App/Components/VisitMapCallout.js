import React from 'react'
import { Text, TouchableOpacity } from 'react-native'
import { Callout } from 'react-native-maps'
import Styles from './Styles/VisitMapCalloutStyles'

const VisitMapCallout = ({ name }) =>
  <Callout style={Styles.callout}>
    <TouchableOpacity>
      <Text>
        {name}
      </Text>
    </TouchableOpacity>
  </Callout>

export default VisitMapCallout
