import { StyleSheet } from 'react-native'
import { Metrics } from '../../Themes'

export default StyleSheet.create({
  sectionHeader: {
    fontSize: 16
  },
  text: {
    fontSize: 14,
    marginBottom: Metrics.marginVertical
  }
})
