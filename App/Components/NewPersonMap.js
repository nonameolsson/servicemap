// @flow
import React from 'react'
import PropTypes from 'prop-types'
import MapView from 'react-native-maps'
import VisitMapCallout from './VisitMapCallout'
import styles from './Styles/NewPersonMapStyle'

// Type
import type { Region } from 'Map'
type Props = {
  region: Region,
  initialRegion: Region,
  title: string
}

type State = {
  region: Region
}

class NewPersonMap extends React.Component<void, Props, State> {
  static propTypes = {
    initialRegion: PropTypes.object.isRequired,
    region: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired
  }

  state = {
    region: {
      latitude: 0,
      longitude: 0,
      latitudeDelta: 0,
      longitudeDelta: 0
    }
  }

  componentWillReceiveProps (nextProps) {
    /* ***********************************************************
    * If you wish to recenter the map on new locations any time the
    * props change, do something like this:
    *************************************************************/
    this.setState({
      region: {
        latitude: nextProps.region.latitude,
        longitude: nextProps.region.longitude,
        latitudeDelta: nextProps.region.latitudeDelta,
        longitudeDelta: nextProps.region.longitudeDelta
      }
    })
  }

  renderMapMarkers = () => {
    const { region, title } = this.props
    return (
      <MapView.Marker
        coordinate={{
          latitude: region.latitude,
          longitude: region.longitude
        }}
        draggable
        onDragEnd={e => this.props.moveMarker(e)}
      >
        <VisitMapCallout name={title} />
      </MapView.Marker>
    )
  }

  render () {
    return (
      <MapView
        style={styles.map}
        initialRegion={this.props.initialRegion}
        region={this.state.region}
        showsUserLocation
      >
        {this.renderMapMarkers()}
      </MapView>
    )
  }
}

export default NewPersonMap
