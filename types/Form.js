declare module 'Form' {
  declare type Result = {
    name: string,
    age: number,
    language: string,
    comments: string,
    latitude: number,
    longitude: number,
    streetName: string,
    streetNumber: number,
    subLocality: string,
    locality: string,
    country: string
  }
}
