// @flow
import React from 'react'
import PropTypes from 'prop-types'
import { Image, View, Text } from 'react-native'

// Styles
import styles from './Styles/EmptyStateStyle'

// Props
type Props = {
  text: string,
  image: string
}

const EmptyState = (props: Props) =>
  <View style={styles.container}>
    <Image
      style={styles.image}
      source={props.image}
      resizeMethod='scale'
      resizeMode='contain'
    />
    <Text style={styles.text}>
      {props.text}
    </Text>
  </View>

export default EmptyState

EmptyState.propTypes = {
  text: PropTypes.string.isRequired,
  image: PropTypes.number.isRequired
}
