import { Platform, StyleSheet } from 'react-native'

export default StyleSheet.create({
  title: {
    fontSize: 20,
    color: '#fff'
  },
  subtitle: {
    textAlign: (Platform.OS === 'ios') ? 'center' : 'left',
    color: '#f0f0f0',
    fontSize: 14
  }
})
