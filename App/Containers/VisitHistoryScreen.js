// @flow
import React from 'react'
import { Alert } from 'react-native'
import { Container, Content, View } from 'native-base'
import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'
import Snackbar from 'react-native-snackbar'

// Custom Components
import VisitNotes from '../Components/VisitNotes'
import Fab from '../Components/FloatingActionButton'
import { HeaderButton } from '../Components/Navigation'

// Lib
import PersonHelper from '../Lib/PersonHelpers'
import PersonNotes from '../Lib/PersonNotes'

// Actions
import PersonsActions from '../Redux/PersonsRedux'

// Styles
import { Colors } from '../Themes'

// Type
type Props = {

}

type State = {
  person: {}
}

class VisitHistoryTab extends React.Component<Props, State> {
  // Initialize navigation options
  static navigationOptions = ({ navigation, screenProps }) => {
    const { params = {} } = navigation.state

    return {
      headerTitle: params.name,
      headerRight: (
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <HeaderButton onPress={() => params.handleRemove()} icon='trash' />
          <HeaderButton onPress={() => params.handleEdit()} icon='create' />
        </View>
      )
    }
  }

  constructor (props) {
    super(props)
    const { id } = this.props.navigation.state.params // Get current person ID
    const persons = this.props.persons // All persons in state
    const currentPerson = PersonHelper(persons, id) // Pick out current person data

    // Initialize state state
    this.state = {
      person: currentPerson
    }
  }

  componentDidMount = () => {
    // Bind component function `handleRemove` to header icon
    this.props.navigation.setParams({
      handleRemove: () => this.removePerson(),
      handleEdit: () => this.navigateToEditPerson()
    })
  };

  componentWillReceiveProps = nextProps => {
    // Person is undefined after deleting it. App will crash if we would try to get the props
    if (nextProps.person !== undefined) {
      const { id } = this.props.navigation.state.params // Get current person ID
      const persons = nextProps.persons // All persons in state
      const currentPerson = PersonHelper(persons, id) // Pick out current person data

      // Use data and populate state with it
      this.setState({ person: currentPerson })

      // if (currentPerson !== undefined) {
      // Update the title with the latest changes, if coming from edit person
      if (this.props.navigation.state.params.name !== currentPerson.name) {
        this.setTitle(currentPerson.name)
      }
      // }
    }
  };

  addNote () {
    const { dispatch } = this.props.navigation
    const { id } = this.props.navigation.state.params

    const NavigationAction = NavigationActions.navigate({
      routeName: 'AddVisitNotesScreen',
      params: {
        id
      }
    })
    dispatch(NavigationAction)
  }

  /**
   * Navigate to edit current person
   */
  navigateToEditPerson = () => {
    this.props.navigation.dispatch(
      NavigationActions.navigate({
        routeName: 'EditPersonScreen',
        params: {
          person: this.state.person
        }
      })
    )
  };

  setTitle = (title: string) => {
    const { dispatch } = this.props.navigation

    const setTitle = NavigationActions.setParams({
      params: {
        name: title
      },
      key: 'VisitHistoryTab'
    })

    dispatch(setTitle)
  };

  /**
   * Show dialog to delete current person
   */
  removePerson = () => {
    const { id } = this.props.navigation.state.params
    const { dispatch } = this.props.navigation
    const { removePerson } = this.props

    Alert.alert(
      'Delete person',
      'Do you really want to delete this person?',
      [
        {
          text: 'Cancel',
          style: 'cancel'
        },
        {
          text: 'Delete',
          onPress: () => {
            Snackbar.show({ title: `Removed ${this.state.person.name}` })
            dispatch(NavigationActions.back())
            removePerson(id)
          }
        }
      ],
      { cancelable: true }
    )
  };

  render () {
    const { id } = this.props.navigation.state.params
    const notes = PersonNotes(this.props.notes, id)

    return (
      <Container>
        <Content>
          <VisitNotes
            notes={notes}
            navigation={this.props.navigation}
          />
        </Content>
        <Fab
          backgroundColor={Colors.secondaryDark}
          icon='add'
          iconColor={Colors.white}
          onPress={() => this.addNote()}
        />
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    notes: state.notes.notes,
    persons: state.persons.persons
  }
}

const mapDispatchToProps = dispatch => {
  return {
    removePerson: id => dispatch(PersonsActions.removePerson(id)),
    herp: id => dispatch(PersonsActions.herp(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(VisitHistoryTab)
