import React from 'react'
import { Platform, StatusBar, View } from 'react-native'
import styles from './Styles/StatusbarStyle'

const CustomStatusbar = () =>
  <View>
    <StatusBar
      backgroundColor='rgba(0, 0, 0, 0.20)'
      style={styles.statusbar}
      barStyle='light-content'
      translucent
      animated
    />
    {Platform.OS === 'android' && Platform.Version >= 20
      ? <View style={styles.statusbar} />
      : null}
  </View>

export default CustomStatusbar

// // Prop type warnings
// Statusbar.propTypes = {
//   someProperty: React.PropTypes.object,
//   someSetting: React.PropTypes.bool.isRequired
// }
//
// // Defaults for props
// Statusbar.defaultProps = {
//   someSetting: false
// }
