// @flow
import React from 'react'
import PropTypes from 'prop-types'
import { View, Text } from 'react-native'

// Styles
import styles from './Styles/HeaderTitleStyle'

type Props = {
  title: string,
  subtitle: string
}

const HeaderTitle = ({ title, subtitle }: Props) =>
  <View>
    <Text style={styles.title}>
      {title}
    </Text>
    <Text style={styles.subtitle}>
      {subtitle}
    </Text>
  </View>

export default HeaderTitle

// Prop type warnings
HeaderTitle.propTypes = {
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired
}
