// @flow
import { Colors } from '../../../Themes'

export default {
  label: {
    color: Colors.primary
  }
}
