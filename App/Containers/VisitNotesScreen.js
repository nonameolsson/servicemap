import React from 'react'
import { Alert } from 'react-native'
import { Text, Container, Content } from 'native-base'
import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'
import I18n from 'react-native-i18n'
import Snackbar from 'react-native-snackbar'

// Custom components
import { HeaderButton } from '../Components/Navigation'
import Fab from '../Components/FloatingActionButton'

// Actions
import NotesActions from '../Redux/NotesRedux'

// Style
import styles from './Styles/VisitNotesStyle'
import { Colors } from '../Themes'

class VisitNotesScren extends React.Component {
  static navigationOptions = ({ navigation, screenProps }) => {
    const { params = {} } = navigation.state
    const title = I18n.l(
      'date.formats.short',
      navigation.state.params.note.date
    )

    return {
      title,
      headerRight: (
        <HeaderButton
          onPress={() => params.handleRemove()}
          icon='trash'
        />
      )
    }
  }

  componentDidMount = () => {
    // Bind component function `handleRemove` to header icon
    this.props.navigation.setParams({
      handleRemove: () => this.removeNote()
    })
  }

  onEditPress = () => {
    const { note } = this.props.navigation.state.params

    this.props.navigation.dispatch(
      NavigationActions.navigate({
        routeName: 'EditNoteScreen',
        params: {
          note,
          key: this.props.navigation.state.key // So we know how to go back from edit screen
        }
      })
    )
  }

  removeNote = () => {
    const { id } = this.props.navigation.state.params.note
    const { dispatch } = this.props.navigation
    const { removeNote } = this.props

    Alert.alert(
      'Delete notes',
      'Do you really want to delete this note?',
      [
        {
          text: 'No',
          style: 'cancel'
        },
        {
          text: 'Delete',
          onPress: () => {
            removeNote(id)
            dispatch(NavigationActions.back())
            Snackbar.show({ title: 'Note deleted' })
          }
        }
      ],
      { cancelable: true }
    )
  }

  render () {
    const { text } = this.props.navigation.state.params.note
    return (
      <Container>
        <Content padder>
          <Text style={styles.sectionHeader}>Note</Text>
          <Text style={styles.text}>
            {text}
          </Text>
        </Content>
        <Fab
          backgroundColor={Colors.secondaryDark}
          icon='create'
          iconColor={Colors.white}
          onPress={() => this.onEditPress()}
        />
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {}
}

const mapDispatchToProps = dispatch => {
  return {
    removeNote: id => dispatch(NotesActions.removeNote(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(VisitNotesScren)
