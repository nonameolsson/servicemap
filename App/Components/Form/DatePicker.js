// @flow
import React from 'react'
import { Item, Input, Label, View, Text } from 'native-base'
import DateTimePicker from 'react-native-modal-datetime-picker'
import I18n from 'react-native-i18n'

export default class MyTextInput extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      isDateTimePickerVisible: false,
      date: I18n.l('date.formats.short', this.props.input.value)
    }
  }

  handleDatePicked = date => {
    const shortDate = I18n.l('date.formats.short', date)

    this.setState({ date: shortDate })
    this.props.input.onChange(date)
    this.hideDateTimePicker()
  }

  hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false })

  showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true })

  render () {
    const { input, meta, ...inputProps } = this.props
    const error = !!(meta.error && meta.touched)

    return (
      <View style={{ marginTop: 16 }}>
        <Item floatingLabel error={error}>
          <Label>
            {this.props.label}
          </Label>
          <Input
            {...inputProps}
            onChangeText={value => input.onChange(value)}
            onBlur={input.onBlur}
            onChange={value => input.onChange(value)}
            onFocus={input.onFocus}
            onTouchStart={this.showDateTimePicker}
            value={this.state.date}
          />
        </Item>
        {meta.touched &&
          meta.error &&
          <Text>
            {meta.error}
          </Text>
        }
        <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
        />
      </View>
    )
  }
}

// // Prop type warnings
// MyTextInput.propTypes = {
//   someProperty: React.PropTypes.object,
//   someSetting: React.PropTypes.bool.isRequired
// }
//
// // Defaults for props
// MyTextInput.defaultProps = {
//   someSetting: false
// }
