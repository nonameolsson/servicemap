const colors = {
  transparent: 'rgba(0,0,0,0)',
  error: 'rgba(200, 0, 0, 0.8)',
  windowTint: 'rgba(0, 0, 0, 0.7)',
  border: '#483F53',

  // Colors
  primary: '#0277bd',
  primaryLight: '#58a5f0',
  primaryDark: '#004c8c',
  secondary: '#a5d6a7',
  secondaryLight: '#d7ffd9',
  secondaryDark: '#75a478',
  white: '#fff',
  black: '#000',
  blue: '#03a9f4',
  cyan: '#31bebb',
  green: '#4caf50',
  orange: '#ff9800',
  red: '#f44336',
  yellow: '#fdd835',

  // App components
  appBackground: '#f8f8f8',
  divider: 'rgba(0, 0, 0, 0.12)',
  systemBar: '#2b7a79',
  listBackground: '#fbfbfb',

  // Stack navigation
  headerBackground: '#369897',
  headerTitle: '#fff',

  // Bottom navigation
  activeLabelColor: 'rgba(2, 119, 189, 0.87)',
  backgroundColor: 'white',
  barBackgroundColor: '#0277bd',
  labelColor: 'rgba(0, 0, 0, 0.54)',
  rippleColor: 'white',
  activeIcon: 'rgba(2, 119, 189, 0.87)',
  icon: 'rgba(0, 0, 0, 0.54)',

  // Text
  textPrimary: 'rgba(0, 0, 0, 0.87)',
  textPrimaryLight: 'rgba(255,255,255,1)',
  textPrimaryDark: 'rgba(0, 0, 0, 0.87)',
  textSecondary: 'rgba(0, 0, 0, 0.54)',
  textSecondaryLight: 'rgba(255,255,255,0.7)',
  textSecondaryDark: 'rgba(0, 0, 0, 0.54)',
  subtitle: 'rgba(23,23,23,0.87)'
}

export default colors
