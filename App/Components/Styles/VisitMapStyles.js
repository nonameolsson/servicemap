import { Dimensions, StyleSheet } from 'react-native'

const width = Dimensions.get('window').width

export default StyleSheet.create({
  map: {
    flex: 1,
    minHeight: 200,
    width
  }
})
