import { Metrics } from '../../../../Themes'

export default {
  button: {
    alignSelf: 'flex-end',
    marginTop: Metrics.marginVertical
  }
}
