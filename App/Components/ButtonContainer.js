import React from 'react'
import { View } from 'react-native'
import styles from './Styles/ButtonContainerStyle'

export default class ButtonContainer extends React.Component {
  render () {
    return (
      <View style={styles.container}>
        {this.props.children}
      </View>
    )
  }
}

// // Prop type warnings
// ButtonContainer.propTypes = {
//   someProperty: React.PropTypes.object,
//   someSetting: React.PropTypes.bool.isRequired
// }
//
// // Defaults for props
// ButtonContainer.defaultProps = {
//   someSetting: false
// }
