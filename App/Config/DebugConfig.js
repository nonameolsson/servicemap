export default {
  useFixtures: false,
  useSentry: !__DEV__,
  ezLogin: false,
  yellowBox: __DEV__,
  reduxLogging: __DEV__,
  includeExamples: __DEV__,
  useReactotron: __DEV__
}
