// @flow
import Geocoder from 'react-native-geocoder'

export default ({
  latitude,
  longitude
}: {
  latitude: number,
  longitude: number
}): Object => {
  if (typeof latitude === 'number' && typeof longitude === 'number') {
    if (latitude !== 0 && longitude !== 0) {
      return new Promise(resolve => {
        // Object must be formatted in this way according to https://github.com/devfd/react-native-geocoder#usage
        const coords: Object = {
          lat: latitude,
          lng: longitude
        }

        const result = Geocoder.geocodePosition(coords)
          .then(res => {
            // TODO: Use try/catch
            // res is an Array of geocoding object (see below)
            return res[0]
          })
          .catch(err => {
            console.tron.log(err)
            return err.message
          })

        resolve(result)
      })
    }
    return 'Not valid coordinates'
  }
}
