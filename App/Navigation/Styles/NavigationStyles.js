// @flow
import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes/'

export default StyleSheet.create({
  header: {
    backgroundColor: Colors.primary
  },
  headerTitle: {
    color: Colors.headerTitle
  },
  card: {
    backgroundColor: Colors.appBackground
  },
  bottomNavigationIcon: {
    color: Colors.icon
  },
  bottomNavigationActiveIcon: {
    color: Colors.activeIcon
  }
})
