// @flow
export default (persons, id) => {
  const person = persons.find(person => {
    return person.id === id
  })

  return person
}
