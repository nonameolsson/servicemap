// @flow
import React from 'react'
import { Item, Input, Label, View, Text } from 'native-base'

// Styles
import styles from './Styles/TextInputStyle'

export default class MyTextInput extends React.Component {
  static defaultProps = {
    keyboardType: 'default'
  }

  render () {
    const { input, meta, editable, ...inputProps } = this.props
    const error = !!(meta.error && meta.touched)

    return (
      <View style={{ marginTop: 16 }}>
        <Item floatingLabel error={error}>
          <Label style={styles.label}>
            {this.props.label}
          </Label>
          <Input
            {...inputProps}
            onChangeText={input.onChange}
            onBlur={input.onBlur}
            onFocus={input.onFocus}
            value={input.value}
            keyboardType={this.props.keyboardType}
          />
        </Item>
        {meta.touched &&
          meta.error &&
          <Text>
            {meta.error}
          </Text>}
      </View>
    )
  }
}

// // Prop type warnings
// MyTextInput.propTypes = {
//   someProperty: React.PropTypes.object,
//   someSetting: React.PropTypes.bool.isRequired
// }
//
// // Defaults for props
// MyTextInput.defaultProps = {
//   someSetting: false
// }
