import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  addNote: ['data'],
  editNote: ['data'],
  removeNote: ['id']
})

export const NotesTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  notes: []
})

/* ------------- Reducers ------------- */

/**
 * Add a new note
 */
export const addNote = (state, { data }) => {
  const { id, personId, date, text } = data

  const note = {
    id,
    personId,
    date,
    text
  }

  return state.merge({ notes: state.notes.concat(note) })
}

/**
 * Edit note
 */
export const editNote = (state, { data }) => {
  const { id, date, text } = data

  const updatedNotes = state.notes.flatMap(note => {
    if (id === note.id) {
      return {
        ...note,
        text,
        date
      }
    } else {
      return note
    }
  })

  return state.merge({ notes: updatedNotes })
}

/**
 * Remove note
 */
export const removeNote = (state, { id }) => {
  const notes = state.notes.filter(note => {
    return note.id !== id
  })

  return state.merge({
    notes
  })
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ADD_NOTE]: addNote,
  [Types.EDIT_NOTE]: editNote,
  [Types.REMOVE_NOTE]: removeNote
})
