import React from 'react'
import { FlatList } from 'react-native'
import { NavigationActions } from 'react-navigation'
import { Text, ListItem, Left, Body, Right } from 'native-base'

// Custom components
import EmptyState from '../Components/EmptyState'

// Styles
import { Images } from '../Themes'
import styles from './Styles/PeopleListStyle'

export default class PeopleList extends React.Component {
  renderItem = ({ item }) => {
    if (item.header) {
      return (
        <ListItem styles={styles.listItem} itemDivider>
          <Left />
          <Body style={{ marginRight: 40 }}>
            <Text style={{ fontWeight: 'bold' }}>
              {item.name}
            </Text>
          </Body>
          <Right />
        </ListItem>
      )
    } else if (!item.header) {
      return (
        <ListItem
          style={styles.listItem}
          onPress={() =>
            this.props.navigation.dispatch(
              NavigationActions.navigate({
                routeName: 'VisitInfo',
                params: {
                  id: item.id,
                  name: item.name
                }
              })
            )}
        >
          <Body>
            <Text>
              {item.name}
            </Text>
          </Body>
        </ListItem>
      )
    }
  }

  render () {
    return (
      <FlatList
        data={this.props.data}
        ListEmptyComponent={
          <EmptyState
            text='You havent added any persons'
            image={Images.emptyStatePersons}
        />
      }
        renderItem={this.renderItem}
        keyExtractor={item => item.id}
      />
    )
  }
}
