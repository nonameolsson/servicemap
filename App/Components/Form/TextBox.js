// @flow
import React from 'react'
import { Item, Input, Label, View, Text } from 'native-base'

// Styles
import styles from './Styles/TextBoxStyle'

export default class TextBox extends React.Component {
  render () {
    const { input, meta, ...inputProps } = this.props
    const error = !!(meta.error && meta.touched)

    return (
      <View style={{ marginTop: 16 }}>
        <Item stackedLabel error={error}>
          <Label style={styles.label}>
            {this.props.label}
          </Label>
          <Input
            {...inputProps}
            style={styles.textbox}
            clearButtonMode='always'
            enablesReturnKeyAutomatically
            multiline
            numberOfLines={5}
            onChangeText={input.onChange}
            onBlur={input.onBlur}
            onFocus={input.onFocus}
            value={input.value}
          />
        </Item>
        {meta.touched &&
          meta.error &&
          <Text>
            {meta.error}
          </Text>}
      </View>
    )
  }
}
