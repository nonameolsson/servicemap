// flow
import React from 'react'
import PropTypes from 'prop-types'
import { View, Text } from 'native-base'

// Custom components
import NewPersonMap from '../../NewPersonMap'

// Styles
import styles from './Styles/Page2Style'

// Type
import type { Region } from 'Map'
type Props = {
  previewAddress: string,
  moveMarker: Function,
  change: Function,
  initialRegion: Region,
  region: Region
}

export default class Page2 extends React.Component<void, Props, void> {
  static propTypes = {
    previewAddress: PropTypes.string.isRequired
  }

  componentDidMount () {
    this.props.navigation.setParams({ title: 'Location' })
  }

  render () {
    const { change, initialRegion, region, previewAddress } = this.props
    return (
      <View style={styles.container}>
        <NewPersonMap
          change={change}
          initialRegion={initialRegion}
          region={region}
          moveMarker={e => this.props.moveMarker(e)}
          title={previewAddress}
        />
        <View style={styles.locationBox}>
          <Text style={{ color: '#0277bd' }}>
            {previewAddress}
          </Text>
        </View>
      </View>
    )
  }
}
