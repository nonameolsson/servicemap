// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  personAdd: ['data'],
  editPerson: ['data'],
  removePerson: ['id']
})

export const PersonsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  persons: []
})

/* ------------- Reducers ------------- */

// request the data from an api
export const addPerson = (state, { data }) => {
  return state.merge({
    persons: state.persons.concat(data)
  })
}

/**
 * Edit person
 */
export const editPerson = (state, { data }) => {
  const { id } = data

  const updatedPersons = state.persons.flatMap(person => {
    if (id === person.id) {
      return {
        ...data
      }
    } else {
      return person
    }
  })

  return state.merge({ persons: updatedPersons })
}

/**
 * Remove person
 */
export const removePerson = (state, { id }) => {
  const persons = state.persons.filter(person => {
    return person.id !== id
  })

  return state.merge({
    persons
  })
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PERSON_ADD]: addPerson,
  [Types.EDIT_PERSON]: editPerson,
  [Types.REMOVE_PERSON]: removePerson
})
