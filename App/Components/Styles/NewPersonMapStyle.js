import { Dimensions, StyleSheet } from 'react-native'

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

export default StyleSheet.create({
  map: {
    flex: 1,
    width,
    height
  }
})
