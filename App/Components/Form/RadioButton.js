// @flow
import React from 'react'
import PropTypes from 'prop-types'
import { Body, Icon, Left, ListItem, Text, Radio, Right } from 'native-base'

// Styles
import styles from './Styles/RadioButtonStyle'

// Type
type Props = {
  label: string,
  val: string,
  selected: string,
  icon?: string,
  changeState: Function,
  input: Object
}

export default class RadioButton extends React.Component<Props, void> {
  static propTypes = {
    label: PropTypes.string.isRequired,
    val: PropTypes.string.isRequired,
    selected: PropTypes.string.isRequired,
    icon: PropTypes.string,
    changeState: PropTypes.func.isRequired,
    input: PropTypes.object.isRequired
  }

  onPress (onChange: Function, val: string, changeState: Function) {
    onChange(val)
    changeState()
  }

  renderIcon (icon: string) {
    if (icon) {
      return (
        <Left>
          <Icon name={icon} />
        </Left>
      )
    }
  }

  render () {
    const { selected, val, icon, label, input, changeState } = this.props
    return (
      <ListItem style={styles.listItem} icon>
        {this.renderIcon(icon)}
        <Body>
          <Text>
            {label}
          </Text>
        </Body>
        <Right>
          <Radio
            {...input}
            onPress={() => this.onPress(input.onChange, val, changeState)}
            selected={selected === val}
          />
        </Right>
      </ListItem>
    )
  }
}
