// @flow
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Button, Text, View } from 'native-base'
import { Field, reduxForm, formValueSelector } from 'redux-form'

// Custom components
import { DatePicker, TextBox } from '../../Form'
import { required } from '../../Form/Validation'

// Style
import styles from './Styles/AddNoteFormStyles'

// Types
type Props = {
  handleSubmit: Function
}

class AddNoteForm extends React.Component<Props, {}> {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired
  }

  submitForm = () => {
    this.props.handleSubmit()
  }

  render () {
    return (
      <View>
        <Field name={'date'} component={DatePicker} label='Date' validate={[required]} />
        <Field autoCapitalize='sentences' name={'text'} component={TextBox} label='Notes' validate={[required]} />
        <Button style={styles.button} onPress={this.props.handleSubmit}>
          <Text>Save</Text>
        </Button>
      </View>
    )
  }
}

let FormComponent = AddNoteForm

// Decorate with redux-form
FormComponent = reduxForm({
  form: 'addNote'
})(AddNoteForm)

// Decorate with connect to read form values
const selector = formValueSelector('addNote') // <-- same as form name
FormComponent = connect(state => {
  // can select values individually
  const {
    date,
    text
  } = selector(
    state,
    'date',
    'text'
  )

  return {
    formResult: {
      date,
      text
    },
    initialValues: {
      date: new Date()
    }
  }
})(FormComponent)

export default FormComponent
