// @flow
import React from 'react'
import { Dimensions } from 'react-native'
import PropTypes from 'prop-types'
import MapView from 'react-native-maps'
import VisitMapCallout from './VisitMapCallout'

// Styles
import Styles from './Styles/VisitMapStyles'

// Types
type Props = {
  location: Object,
  name: string
}

type State = {
  region: {
    latitude: number,
    longitude: number,
    latitudeDelta: number,
    longitudeDelta: number
  }
}

class VisitMap extends React.Component<Props, State> {
  static propTypes = {
    location: PropTypes.object,
    name: PropTypes.string
  }

  constructor (props) {
    super(props)

    const region = {
      latitude: this.props.location.latitude,
      longitude: this.props.location.longitude,
      latitudeDelta: 0.01,
      longitudeDelta: 0.01
    }

    this.state = {
      region
    }
  }

  renderMapMarkers = () => {
    const { location, name } = this.props
    return (
      <MapView.Marker
        coordinate={{
          latitude: location.latitude,
          longitude: location.longitude
        }}
      >
        <VisitMapCallout name={name} />
      </MapView.Marker>
    )
  }

  render () {
    return (
      <MapView
        style={Styles.map}
        initialRegion={this.state.region}
        showsUserLocation
      >
        {this.renderMapMarkers()}
      </MapView>
    )
  }
}

export default VisitMap
