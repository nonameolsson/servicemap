// @flow
import React from 'react'
import PropTypes from 'prop-types'
import { Alert } from 'react-native'
import { NavigationActions } from 'react-navigation'
import Snackbar from 'react-native-snackbar'
import {
  Body,
  Container,
  Content,
  Icon,
  Left,
  List,
  ListItem,
  Separator,
  Text,
  View
} from 'native-base'
import { Row, Grid } from 'react-native-easy-grid'
import { connect } from 'react-redux'

// Custom components
import VisitMap from '../Components/VisitMap'
import { HeaderButton } from '../Components/Navigation'

// Lib
import PersonHelper from '../Lib/PersonHelpers'

// Actions
import PersonsActions from '../Redux/PersonsRedux'

// Style
import styles from './Styles/VisitInfoScreenStyle'

// Type
type Props = {

}

type State = {
  person: {}
}

class VisitInfoTab extends React.Component<Props, State> {
  // Initialize navigation options
  static navigationOptions = ({ navigation, screenProps }) => {
    const { params = {} } = navigation.state

    return {
      headerTitle: params.name,
      headerRight: (
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <HeaderButton onPress={() => params.handleRemove()} icon='trash' />
          <HeaderButton onPress={() => params.handleEdit()} icon='create' />
        </View>
      )
    }
  }

  constructor (props) {
    super(props)
    const { id } = this.props.navigation.state.params // Get current person ID
    const persons = this.props.persons // All persons in state
    const currentPerson = PersonHelper(persons, id) // Pick out current person data

    // Initialize state state
    this.state = {
      person: currentPerson
    }
  }

  componentDidMount = () => {
    // Bind component function `handleRemove` to header icon
    this.props.navigation.setParams({
      handleRemove: () => this.removePerson(),
      handleEdit: () => this.navigateToEditPerson()
    })
  };

  componentWillReceiveProps = nextProps => {
    // Person is undefined after deleting it. App will crash if we would try to get the props
    if (nextProps.person !== undefined) {
      const { id } = this.props.navigation.state.params // Get current person ID
      const persons = nextProps.persons // All persons in state
      const currentPerson = PersonHelper(persons, id) // Pick out current person data

      // Use data and populate state with it
      this.setState({ person: currentPerson })

      // Update the title with the latest changes, if coming from edit person
      if (this.props.navigation.state.params.name !== currentPerson.name) {
        this.setTitle(currentPerson.name)
      }
    }
  };

  /**
   * Navigate to edit current person
   */
  navigateToEditPerson = () => {
    this.props.navigation.dispatch(
      NavigationActions.navigate({
        routeName: 'EditPersonScreen',
        params: {
          person: this.state.person
        }
      })
    )
  };

  /**
   * Show dialog to delete current person
   */
  removePerson = () => {
    const { id } = this.props.navigation.state.params
    const { dispatch } = this.props.navigation
    const { removePerson } = this.props

    Alert.alert(
      'Delete person',
      'Do you really want to delete this person?',
      [
        {
          text: 'Cancel',
          style: 'cancel'
        },
        {
          text: 'Delete',
          onPress: () => {
            Snackbar.show({ title: `Removed ${this.state.person.name}` })
            dispatch(NavigationActions.back())
            removePerson(id)
          }
        }
      ],
      { cancelable: true }
    )
  };

  renderLocation = () => {
    const { location } = this.state.person

    if (location !== undefined) {
      return (
        <Text>
          {location.streetName} {location.streetNumber}
          , {location.subLocality}, {location.locality}
        </Text>
      )
    } else {
      return ''
    }
  }

  renderVisitMap = () => {
    const { location, name } = this.state.person

    if (location !== undefined) {
      return <VisitMap location={location} name={name} />
    }

    return null
  };

  setTitle = (title: string) => {
    const { dispatch } = this.props.navigation

    const setTitle = NavigationActions.setParams({
      params: {
        name: title
      },
      key: 'VisitHistoryTab'
    })

    dispatch(setTitle)
  };

  render () {
    const { name, age, comments } = this.state.person

    return (
      <Container>
        <Content>
          <Grid>
            <Row>
              <List style={styles.list}>
                <Separator>
                  <Text>VISIT INFORMATION</Text>
                </Separator>

                <ListItem icon>
                  <Left>
                    <Icon name='person' />
                  </Left>
                  <Body>
                    <Text>Name: {name}</Text>
                  </Body>
                </ListItem>
                <ListItem>
                  <Text style={{ paddingLeft: 42 }}>Age: {age}</Text>
                </ListItem>

                <Separator>
                  <Text>COMMENTS</Text>
                </Separator>
                <ListItem icon>
                  <Left>
                    <Icon name='text' />
                  </Left>
                  <Body>
                    <Text>{comments}</Text>
                  </Body>
                </ListItem>

                <Separator>
                  <Text>LOCATION</Text>
                </Separator>
                <ListItem icon>
                  <Left>
                    <Icon name='pin' />
                  </Left>
                  <Body>{this.renderLocation()}</Body>
                </ListItem>
              </List>
            </Row>
            <Row>{this.renderVisitMap()}</Row>
          </Grid>
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    persons: state.persons.persons
  }
}

const mapDispatchToProps = dispatch => {
  return {
    removePerson: id => dispatch(PersonsActions.removePerson(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(VisitInfoTab)
