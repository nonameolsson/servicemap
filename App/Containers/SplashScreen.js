// @flow
import React from 'react'

class SplashScreen extends React.Component<{}, {}> {
  componentDidMount () {
    // do stuff while splash screen is shown
    // After having done stuff (such as async tasks) hide the splash screen
    SplashScreen.hide()
  }
}

export default SplashScreen
