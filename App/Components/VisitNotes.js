// @flow
import React from 'react'
import PropTypes from 'prop-types'
import { FlatList } from 'react-native'
import {
  Body,
  List,
  ListItem,
  Text
} from 'native-base'
import I18n from 'react-native-i18n'

// Custom components
import EmptyState from '../Components/EmptyState'

// Libs
import NoteHelpers from '../Lib/NoteHelpers'

// Styles
import { Images } from '../Themes'
import styles from './Styles/VisitNotesStyle'

// Types
type Props = {
  notes: Array<Object>,
  navigation: Object
}

export default class VisitNotes extends React.Component<Props, {}> {
  static propTypes = {
    notes: PropTypes.array.isRequired,
    navigation: PropTypes.object.isRequired
  }

  onPress = (note) => {
    this.props.navigation.navigate('VisitNotesScreen', { note })
  }

  renderItem = ({ item }) => {
    return (
      <ListItem
        style={styles.listItem}
        onPress={() => this.onPress(item)}
      >
        <Body>
          <Text date>
            {I18n.l('date.formats.short', item.date)}
          </Text>
          <Text note numberOfLines={1} ellipsizeMode='tail'>
            {item.text}
          </Text>
        </Body>
      </ListItem>
    )
  }

  render () {
    const notes = NoteHelpers.sortNotes(this.props.notes)

    return (
      <List>
        <FlatList
          ListEmptyComponent={
            <EmptyState
              text='You havent added any notes yet'
              image={Images.emptyStateNotes}
              />
            }
          data={notes}
          renderItem={item => this.renderItem(item)}
          keyExtractor={item => item.id}
        />
      </List>
    )
  }
}
