import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    alignSelf: 'center',
    flexDirection: 'row',
    flex: 1,
    height: 48,
    justifyContent: 'space-around',
    width: 48
  },
  icon: {
    alignSelf: 'center'
  }
})
