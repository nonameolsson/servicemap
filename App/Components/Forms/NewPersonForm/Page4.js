// @flow
import React from 'react'
import { Content, Text, List, ListItem, Right, View } from 'native-base'

// Styles
import styles from './Styles/Page4Style'

// Type
type Props = {
  formResult: Object
}

export default class Page4 extends React.Component<Props, {}> {
  componentDidMount () {
    this.props.navigation.setParams({ title: 'Review' })
  }

  render () {
    const { formResult } = this.props

    return (
      <Content>
        <List style={styles.list}>
          <ListItem itemHeader itemDivider>
            <Text>Personal Details</Text>
          </ListItem>
          <ListItem>
            <Text>Name</Text>
            <Right>
              <Text>
                {formResult.name}
              </Text>
            </Right>
          </ListItem>
          <ListItem>
            <Text>Age</Text>
            <Right>
              <Text>
                {formResult.age}
              </Text>
            </Right>
          </ListItem>
          <ListItem>
            <Text>Language</Text>
            <Right>
              <Text>
                {formResult.language}
              </Text>
            </Right>
          </ListItem>
          <ListItem itemHeader itemDivider>
            <Text>Location</Text>
          </ListItem>
          <ListItem>
            <Text>{`${formResult.streetName} ${formResult.streetNumber}`}</Text>
          </ListItem>
          <ListItem>
            <Text>{`${formResult.subLocality} ${formResult.locality}`}</Text>
          </ListItem>
          <ListItem>
            <Text>
              {formResult.country}
            </Text>
          </ListItem>
          {formResult.comments
            ? <View>
              <ListItem itemHeader itemDivider>
                <Text>Comments</Text>
              </ListItem>
              <ListItem>
                <Text>
                  {formResult.comments}
                </Text>
              </ListItem>
            </View>
            : null}
        </List>
      </Content>
    )
  }
}

// // Prop type warnings
// Page3.propTypes = {
//   someProperty: React.PropTypes.object,
//   someSetting: React.PropTypes.bool.isRequired
// }
//
// // Defaults for props
// Page3.defaultProps = {
//   someSetting: false
// }
