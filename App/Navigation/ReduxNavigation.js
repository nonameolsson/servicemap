// @flow
import React from 'react'
import PropTypes from 'prop-types'
import { BackHandler } from 'react-native'

import { addNavigationHelpers, NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'
import AppNavigation from './AppNavigation'

// Types
type Routes = [
  {
    index: number,
    routeName: string,
    routes: Array<Object>
  }
];

type Nav = {
  index: number,
  routes: Routes
};

type Props = {
  dispatch: Function,
  nav: Nav
};

type Navigation = {
  dispatch: Function,
  goBack: Function,
  navigate: Function,
  setParams: Function,
  state: Object
};

// here is our redux-aware our smart component
class ReduxNavigation extends React.Component<Props, {}> {
  initialNav: Object

  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    nav: PropTypes.object.isRequired
  }

  componentDidMount () {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress)
  }

  componentWillUnmount () {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress)
  }

  onBackPress = (): boolean => {
    const { dispatch, nav } = this.props
    if (nav.index === 0) {
      return false
    }

    dispatch(NavigationActions.back())
    return true
  }

  render () {
    const { dispatch, nav }: Props = this.props
    const navigation: Navigation = addNavigationHelpers({
      dispatch,
      state: nav
    })

    return <AppNavigation navigation={navigation} />
  }
}

const mapStateToProps = state => ({ nav: state.nav })
export default connect(mapStateToProps)(ReduxNavigation)
