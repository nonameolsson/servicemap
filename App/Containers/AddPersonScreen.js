// @flow
import React from 'react'
import PropTypes from 'prop-types'
import { Alert } from 'react-native'
import { connect } from 'react-redux'
import { Container } from 'native-base'
import { v1 } from 'uuid'
import { NavigationActions } from 'react-navigation'
import Snackbar from 'react-native-snackbar'

// Custom components
import { NewPersonForm } from '../Components/Forms/NewPersonForm/'
import HeaderTitle from '../Components/HeaderTitle'
import { HeaderButton } from '../Components/Navigation'

// Actions
import PersonActions from '../Redux/PersonsRedux'

// Styles
// import styles from './Styles/AddPersonScreenStyle'

// Types
import type { Region } from 'Map'
type Person = {
  name: string,
  gender: string,
  latitude: number,
  longitude: number,
  streetName: string,
  streetNumber: number,
  locality: string,
  country: string
}

type Props = {
  form?: {
    values: Object
  },
  navigation: {
    setParams: Function,
    state: {
      params: {
        region: Region
      }
    }
  },
  addPerson: Function
}

type State = {
  index: number,
  region: Region
}

class AddPersonScreen extends React.Component<Props, State> {
  static propTypes = {
    form: PropTypes.object,
    navigation: PropTypes.object.isRequired,
    addPerson: PropTypes.func.isRequired
  };

  static navigationOptions = ({ navigation, screenProps }) => {
    const { params = {} } = navigation.state

    return {
      headerTitle: (
        <HeaderTitle
          title='Add new person'
          subtitle={navigation.state.params.subtitle}
        />
      ),
      headerLeft: (
        <HeaderButton
          onPress={() => params.previousScreen()}
          icon='arrow-back'
          title='back'
        />
      ),
      headerRight: (
        <HeaderButton
          onPress={() => params.nextScreen()}
          icon='arrow-forward'
          title='next'
        />
      )
    }
  }

  constructor (props) {
    super(props)

    const { region } = props.navigation.state.params

    this.state = {
      index: 0,
      region: {
        ...region
      }
    }
  }

  componentDidMount = () => {
    // Bind component function `handleRemove` to header icon
    this.props.navigation.setParams({
      previousScreen: () => this.previousScreen(),
      nextScreen: () => this.nextScreen()
    })
  }

  previousScreen = () => {
    const backAction = NavigationActions.back()

    if (this.state.index > 0) {
      this.setState({
        index: this.state.index - 1
      })
    } else {
      Alert.alert(
        'Discard changes',
        'Do you want to discard the changes?',
        [
          {
            text: 'No',
            style: 'cancel'
          },
          {
            text: 'Discard',
            onPress: () => {
              this.props.navigation.dispatch(backAction)
            }
          }
        ],
        { cancelable: true }
      )
    }
  }

  nextScreen = () => {
    const { values } = this.props.form
    const { index } = this.state

    if (index < 3) {
      this.setState({
        index: index + 1
      })
    } else if (index === 3) {
      this.save(values)
      Snackbar.show({ title: `${values.name} was added` })
    }
  }

  onIndexChanged = (index: number) => {
    this.setState({
      index
    })

    switch (index) {
      case 0:
        this.props.navigation.setParams({ subtitle: 'Details' })
        break

      case 1:
        this.props.navigation.setParams({ subtitle: 'Map' })
        break

      case 2:
        this.props.navigation.setParams({ subtitle: 'Address' })
        break

      case 3:
        this.props.navigation.setParams({ subtitle: 'Review' })
        break
    }
  }

  save = (data: Person) => {
    const person = {
      id: v1(),
      name: data.name,
      age: data.age,
      gender: data.gender,
      language: data.language,
      comments: data.comments,
      location: {
        latitude: parseFloat(data.latitude),
        longitude: parseFloat(data.longitude),
        streetName: data.streetName,
        streetNumber: parseInt(data.streetNumber),
        locality: data.locality,
        country: data.country
      }
    }

    this.props.addPerson(person)
    this.props.navigation.dispatch(NavigationActions.back())
  }

  render () {
    return (
      <Container>
        <NewPersonForm
          index={this.state.index}
          initialRegion={this.props.navigation.state.params.region}
          navigation={this.props.navigation}
          onIndexChanged={this.onIndexChanged}
          onSubmit={() => this.save(this.props.form.values)}
          region={this.props.navigation.state.params.region}
        />
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    form: state.form.addNewPerson
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addPerson: person => dispatch(PersonActions.personAdd(person))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddPersonScreen)
