import { combineReducers } from 'redux'
import configureStore from './CreateStore'
import rootSaga from '../Sagas/'
import { reducer as formReducer } from 'redux-form'

export default () => {
  /* ------------- Assemble The Reducers ------------- */
  const rootReducer = combineReducers({
    form: formReducer,
    location: require('./LocationRedux').reducer,
    nav: require('./NavigationRedux').reducer,
    notes: require('./NotesRedux').reducer,
    persons: require('./PersonsRedux').reducer
  })

  return configureStore(rootReducer, rootSaga)
}
