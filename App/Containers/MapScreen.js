// @flow
import React from 'react'
import { Alert, Dimensions, View } from 'react-native'
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'
import SplashScreen from 'react-native-splash-screen'

// Custom components
import Map from '../Components/Map'
import Fab from '../Components/FloatingActionButton'

// Add Actions - replace 'Your' with whatever your reducer is called :)
import LocationActions from '../Redux/LocationRedux'

// Styles
import styles from './Styles/MapScreenStyle'
import { Colors } from '../Themes'

// Types
type State = {
  initialRegion: {
    latitude: number,
    longitude: number,
    latitudeDelta: number,
    longitudeDelta: number
  },
  region: {
    latitude: number,
    longitude: number,
    latitudeDelta: number,
    longitudeDelta: number
  }
};

const { width, height } = Dimensions.get('window')

const ASPECT_RATIO = width / height
const LATITUDE_DELTA = 0.01
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO

class MapScreen extends React.Component<{}, State> {
  state = {
    initialRegion: {
      latitude: 0,
      longitude: 0,
      latitudeDelta: 0,
      longitudeDelta: 0
    },
    region: {
      latitude: 0,
      longitude: 0,
      latitudeDelta: 0,
      longitudeDelta: 0
    }
  }

  componentDidMount () {
    SplashScreen.hide()

    navigator.geolocation.getCurrentPosition(
      position => {
        let lat = parseFloat(position.coords.latitude)
        let long = parseFloat(position.coords.longitude)

        let initialRegion = {
          latitude: lat,
          longitude: long,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA
        }

        this.setState({ initialRegion })
        this.props.updateLocation(initialRegion)
      },
      error => Alert.alert(JSON.stringify(error)),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    )

    this.watchID = navigator.geolocation.watchPosition(position => {
      let lat = parseFloat(position.coords.latitude)
      let long = parseFloat(position.coords.longitude)

      let lastRegion = {
        latitude: lat,
        longitude: long,
        longitudeDelta: LONGITUDE_DELTA,
        latitudeDelta: LATITUDE_DELTA
      }

      this.setState({ region: lastRegion })
      this.props.updateLocation(lastRegion)
    })
  }

  componentWillUnmount () {
    navigator.geolocation.clearWatch(this.watchID)
  }

  onPress () {
    const { dispatch } = this.props.navigation
    const NavigationAction = NavigationActions.navigate({
      routeName: 'AddPerson',
      params: { subtitle: 'Details', region: this.props.location }
    })
    dispatch(NavigationAction)
  }

  render () {
    return (
      <View style={styles.container}>
        <Map
          initialRegion={this.state.initialRegion}
          region={this.state.region}
          persons={this.props.persons}
          navigation={this.props.navigation}
        />
        <Fab
          backgroundColor={Colors.secondaryDark}
          icon='add'
          iconColor={Colors.white}
          onPress={() => this.onPress()}
        />
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    persons: state.persons.persons,
    location: state.location.region
  }
}

const mapDispatchToProps = dispatch => {
  return {
    updateLocation: region => dispatch(LocationActions.updateLocation(region))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MapScreen)
