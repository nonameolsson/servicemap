// @flow
import React from 'react'
import PropTypes from 'prop-types'
import { View, TouchableOpacity } from 'react-native'
import { Icon } from 'native-base'

// Styles
import styles from './Styles/HeaderButtonStyle'

type Props = {
  onPress: Function,
  icon: string,
  title?: string,
  color?: string,
  fontSize?: number
};

const HeaderButton = ({ onPress, icon, color, fontSize }: Props) =>
  <TouchableOpacity onPress={onPress}>
    <View style={styles.container}>
      <Icon style={[styles.icon, { color, fontSize }]} name={icon} />
    </View>
  </TouchableOpacity>

export default HeaderButton

// Prop type warnings
HeaderButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  icon: PropTypes.string,
  color: PropTypes.string,
  title: PropTypes.string,
  fontSize: PropTypes.number
}

// Defaults for props
HeaderButton.defaultProps = {
  color: '#fff',
  fontSize: 27
}
