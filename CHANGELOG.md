# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)

## [Unreleased]

### Added
- Nothing

### Changed
- Nothing

### Bugs
- Nothing

## [0.0.2] - 2017-11-05
Initial version with NPM Versioning
