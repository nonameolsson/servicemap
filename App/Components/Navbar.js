import React from 'react'
import { Body, Header, Title } from 'native-base'

export default class Navbar extends React.Component {
  render () {
    const { getScreenDetails, scene } = this.props
    const details = getScreenDetails(scene)

    return (
      <Header hasTabs>
        <Body>
          <Title>
            {details.options.title}
          </Title>
        </Body>
      </Header>
    )
  }
}

// Prop type warnings
Navbar.propTypes = {
  getScreenDetails: React.PropTypes.func.isRequired,
  scene: React.PropTypes.object.isRequired
}
