// @flow
import { StyleSheet } from 'react-native'
import { Colors } from '../../../Themes'

export default StyleSheet.create({
  label: {
    color: Colors.primary
  },
  textbox: {
    borderColor: '#d9d5dc',
    borderWidth: 1,
    textAlignVertical: 'top',
    marginTop: 16,
    paddingHorizontal: 8
  }
})
