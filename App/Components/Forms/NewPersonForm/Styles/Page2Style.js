import { Dimensions, StyleSheet } from 'react-native'

const { width } = Dimensions.get('window')

export default StyleSheet.create({
  container: {
    flex: 1
  },
  locationBox: {
    alignItems: 'center',
    elevation: 2,
    backgroundColor: 'rgba(255, 255, 255,1)',
    position: 'absolute',
    bottom: 70,
    alignSelf: 'center',
    padding: 10,
    marginHorizontal: 10,
    shadowColor: '#000',
    shadowRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: { x: 2, y: -2 },
    overflow: 'hidden',
    marginLeft: 16,
    width: width - 16
  }
})
