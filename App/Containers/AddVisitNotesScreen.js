import React from 'react'
import { Content, Container } from 'native-base'
import { connect } from 'react-redux'
import { v1 } from 'uuid'
import { NavigationActions } from 'react-navigation'
import Snackbar from 'react-native-snackbar'
import { submit } from 'redux-form'

// Custom components
import AddNoteForm from '../Components/Forms/AddNoteForm'

// Add Actions - replace 'Your' with whatever your reducer is called :)
import NotesActions from '../Redux/NotesRedux'

class AddVisitNotesScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: 'Add note'
  })

  saveNote = () => {
    const { date, text } = this.props.form.values
    const { addNote } = this.props
    const personId = this.props.navigation.state.params.id
    const { dispatch } = this.props.navigation

    const newNote = {
      id: v1(),
      personId,
      date,
      text
    }

    addNote(newNote)
    Snackbar.show({ title: 'Note saved!' })
    dispatch(NavigationActions.back())
  }

  render () {
    return (
      <Container>
        <Content padder>
          <AddNoteForm onSubmit={this.saveNote} />
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    form: state.form.addNote
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addNote: newNote => dispatch(NotesActions.addNote(newNote))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddVisitNotesScreen)
