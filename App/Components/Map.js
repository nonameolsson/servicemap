// @flow
import React from 'react'
import PropTypes from 'prop-types'
import { NavigationActions } from 'react-navigation'
import MapView from 'react-native-maps'
import MapCallout from './MapCallout'
import Styles from './Styles/MapStyles'

type Position = {
  latitude: number,
  longitude: number,
  latitudeDelta: number,
  longitudeDelta: number
}

type Props = {
  position: Position,
  persons: Array<Object>
}

type State = {
  region: Position,
  locations: Array<Object>,
  loadingEnabled: boolean,
  fitToElements: boolean
}

class Map extends React.Component<Props, State> {
  static propTypes = {
    initialRegion: PropTypes.object.isRequired,
    region: PropTypes.object.isRequired,
    persons: PropTypes.array.isRequired
  }

  state = {
    region: {
      latitude: 0,
      longitude: 0,
      latitudeDelta: 0,
      longitudeDelta: 0
    }
  }

  componentWillReceiveProps (nextProps) {
    /* ***********************************************************
    * If you wish to recenter the map on new locations any time the
    * props change, do something like this:
    *************************************************************/
    this.setState({
      region: {
        latitude: nextProps.region.latitude,
        longitude: nextProps.region.longitude,
        latitudeDelta: nextProps.region.latitudeDelta,
        longitudeDelta: nextProps.region.longitudeDelta
      }
    })
  }

  calloutPress (person) {
    const { id, name } = person

    this.props.navigation.dispatch(
      NavigationActions.navigate({
        routeName: 'VisitInfo',
        params: {
          id,
          name
        }
      })
    )
  }

  renderMapMarkers = person => {
    const { id, location, name } = person

    return (
      <MapView.Marker
        key={id}
        coordinate={{
          latitude: location.latitude,
          longitude: location.longitude
        }}
      >
        <MapCallout name={name} onPress={() => this.calloutPress(person)} />
      </MapView.Marker>
    )
  }

  render () {
    return (
      <MapView
        style={Styles.map}
        initalRegion={this.props.initialRegion}
        region={this.state.region}
        showsUserLocation
      >
        {this.props.persons.map(person => this.renderMapMarkers(person))}
      </MapView>
    )
  }
}

export default Map
