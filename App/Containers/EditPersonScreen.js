// @flow
import React from 'react'
import { Container, Content } from 'native-base'
import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'
import Snackbar from 'react-native-snackbar'

// Custom components
import { HeaderButton } from '../Components/Navigation'
import EditPersonForm from '../Components/Forms/EditPersonForm'

// Actions
import PersonActions from '../Redux/PersonsRedux'

// Types
type Props = {
  editPerson: Function,
  navigation: {
    state: {
      params: {
        person: {
          id: string,
          name: string,
          age: number,
          gender: string,
          language: string,
          location: {
            latitude: number,
            longitude: number,
            streetName: string,
            streetNumber: number,
            locality: string,
            country: string
          }
        }
      }
    }
  }
};

class EditPersonScreen extends React.Component<Props, void> {
  static navigationOptions = ({ navigation, screenProps }) => {
    const { params = {} } = navigation.state
    return {
      headerTitle: params.person.name,
      // headerTitle: 'Edit person',
      headerRight: (
        <HeaderButton onPress={() => params.handleSave()} icon='checkmark' />
      )
    }
  };

  componentDidMount = () => {
    // Bind component function `handleRemove` to header icon
    this.props.navigation.setParams({
      handleSave: () => this.saveChanges()
    })
  }

  // Update person with changes
  saveChanges = () => {
    const { dispatch } = this.props.navigation
    const person = this.props.person.values

    // Save changes to Redux
    this.props.editPerson(person)

    // Prepare actions
    const backAction = NavigationActions.back()

    // Dispatch actions
    dispatch(backAction)

    // Show a pretty message to the user
    Snackbar.show({ title: `${person.name} was updated` })
  }

  render () {
    const { person } = this.props.navigation.state.params

    return (
      <Container>
        <Content padder>
          <EditPersonForm person={person} />
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    person: state.form.editPerson
  }
}

const mapDispatchToProps = dispatch => {
  return {
    editPerson: person => dispatch(PersonActions.editPerson(person))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditPersonScreen)
