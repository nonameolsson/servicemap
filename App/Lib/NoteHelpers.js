// @flow
import Immutable from 'seamless-immutable'

const sortNotes = (notes: Array<object>) => {
  let sortedArr = Immutable.asMutable(notes)
  sortedArr.sort()

  // Function to sort object based on key
  const compare = (a, b) => {
    const dateA = a.date
    const dateB = b.date

    let comparison = 0
    if (dateA > dateB) {
      comparison = 1
    } else if (dateA < dateB) {
      comparison = -1
    }

    return comparison
  }

  // Sort the array
  sortedArr.sort(compare)

  return sortedArr
}

export default {
  sortNotes
}
