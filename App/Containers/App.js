import '../Config'
import DebugConfig from '../Config/DebugConfig'
import React, { Component } from 'react'
import { Provider } from 'react-redux'

import { Sentry, SentryLog } from 'react-native-sentry'

// Custom components
import RootContainer from './RootContainer'
import createStore from '../Redux'

// create our store
const store = createStore()

if (DebugConfig.useSentry) {
  // disable stacktrace merging
  Sentry.config(
    'https://a7d1968cfbc145658203c7ddc8e6e3e7:1c13d304c48a4a6aa3151a90dfa1939f@sentry.io/221374', {
      deactivateStacktraceMerging: true, // default: false | Deactivates the stacktrace merging feature
      logLevel: SentryLog.Debug, // default SentryLog.None | Possible values:  .None, .Error, .Debug, .Verbose
      disableNativeIntegration: false // default: false | Deactivates the native integration and only uses raven-js
      // sampleRate: 0.5 // default: 1.0 | Only set this if you don't want to send every event so e.g.: 0.5 will send 50% of all events
      // These two options will only be considered if stacktrace merging is active
      // Here you can add modules that should be ignored or exclude modules
      // that should no longer be ignored from stacktrace merging
      // ignoreModulesExclude: ["I18nManager"], // default: [] | Exclude is always stronger than include
      // ignoreModulesInclude: ["RNSentry"], // default: [] | Include modules that should be ignored too
      // ---------------------------------
    }
  ).install()

  // set the tag context
  Sentry.setTagsContext({
    'environment': 'staging',
    'react': true
  })
}
/**
 * Provides an entry point into our application.  Both index.ios.js and index.android.js
 * call this component first.
 *
 * We create our Redux store here, put it into a provider and then bring in our
 * RootContainer.
 *
 * We separate like this to play nice with React Native's hot reloading.
 */
class App extends Component {
  render () {
    return (
      <Provider store={store}>
        <RootContainer />
      </Provider>
    )
  }
}

// allow reactotron overlay for fast design in dev mode
export default (DebugConfig.useReactotron ? console.tron.overlay(App) : App)
