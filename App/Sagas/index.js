import { takeLatest } from 'redux-saga/effects'

/* ------------- Types ------------- */
import { StartupTypes } from '../Redux/StartupRedux'

/* ------------- Sagas ------------- */
import { startup } from './StartupSagas'

/* ------------- Connect Types To Sagas ------------- */
export default function * root () {
  yield [
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup)
  ]
}
