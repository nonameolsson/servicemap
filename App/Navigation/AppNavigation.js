// @flow
import { StackNavigator } from 'react-navigation'

// Screens
import AddPersonScreen from '../Containers/AddPersonScreen'
import AddVisitNotesScreen from '../Containers/AddVisitNotesScreen'
import EditNoteScreen from '../Containers/EditNoteScreen'
import EditPersonScreen from '../Containers/EditPersonScreen'
import VisitNotesScreen from '../Containers/VisitNotesScreen'

// Tabs
import PrimaryTabs from './PrimaryTabs'
import VisitTabs from './VisitTabs'

// Custom styles
import styles from './Styles/NavigationStyles'
import Colors from '../Themes/Colors'

/* -----------
  Main navigator
  Manifest of possible screens
  ----------- */

const appNavRouteConfigs = {
  Home: {
    screen: PrimaryTabs,
    navigationOptions: {
      title: 'ServiceMap'
    }
  },
  VisitInfo: {
    screen: VisitTabs
  },
  VisitNotesScreen: {
    screen: VisitNotesScreen
  },
  AddPerson: {
    screen: AddPersonScreen
  },
  EditPersonScreen: {
    screen: EditPersonScreen
  },
  AddVisitNotesScreen: {
    screen: AddVisitNotesScreen
  },
  EditNoteScreen: {
    screen: EditNoteScreen
  }
}

const appNavConfig = {
  cardStyle: styles.card,
  navigationOptions: {
    headerStyle: styles.header,
    headerTintColor: Colors.white
  }
}

const AppNav = StackNavigator(appNavRouteConfigs, appNavConfig)

export default AppNav
