// @flow
import React from 'react'
import { TabNavigator } from 'react-navigation'
import { NavigationComponent } from 'react-native-material-bottom-navigation'
import { Icon } from 'native-base'

// Screens
import MapScreen from '../Containers/MapScreen'
import PeopleScreen from '../Containers/PeopleScreen'

// Custom styles
import styles from './Styles/NavigationStyles'
import Colors from '../Themes/Colors'

const primaryTabsRoutes = {
  Map: {
    screen: MapScreen,
    navigationOptions: {
      tabBarLabel: 'Map'
    }
  },
  People: {
    screen: PeopleScreen,
    navigationOptions: {
      tabBarLabel: 'People'
    }
  }
}

const primaryTabsConfig = {
  animationEnabled: true,
  tabBarComponent: NavigationComponent,
  tabBarPosition: 'bottom',
  backBehavior: 'none',
  tabBarOptions: {
    bottomNavigationOptions: {
      style: { borderTopWidth: 0, elevation: 8 },
      activeLabelColor: Colors.activeLabelColor,
      backgroundColor: Colors.backgroundColor,
      labelColor: Colors.labelColor,
      rippleColor: Colors.primary,
      tabs: {
        Map: {
          activeIcon: (
            <Icon name='map' style={styles.bottomNavigationActiveIcon} />
          ),
          icon: <Icon name='map' style={styles.bottomNavigationIcon} />
        },
        People: {
          activeIcon: (
            <Icon name='people' style={styles.bottomNavigationActiveIcon} />
          ),
          icon: <Icon name='people' style={styles.bottomNavigationIcon} />
        }
      }
    }
  }
}

export default TabNavigator(primaryTabsRoutes, primaryTabsConfig)
