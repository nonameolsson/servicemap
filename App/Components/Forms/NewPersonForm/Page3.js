// flow
import React from 'react'
import { View } from 'react-native'
import { Button, Content, H2, Icon, Text } from 'native-base'
import { Col, Grid } from 'react-native-easy-grid'
import { Field } from 'redux-form'

// Custom components
import { TextInput, HiddenComponent } from '../../Form'
import { required, number } from '../../Form/Validation'

// Libs
import GetAddress from '../../../Lib/GetAddress'

// Styles
import styles from './Styles/Page3Style'

// Types
import type { Result } from 'Form'
type State = {
  buttonDisabled: boolean
}

type Props = {
  formResult: Result,
  navigation: Object
}

export default class Page3 extends React.Component<Props, State> {
  componentDidMount () {
    this.props.navigation.setParams({ title: 'Location details' })
  }

  render () {
    console.tron.log(this.props)
    return (
      <Content padder>
        <View style={styles.section}>
          <H2>Address details</H2>
        </View>
        <View
          style={[
            styles.section,
            { borderTopColor: 'rgba(0, 0, 0, 0.12)', borderTopWidth: 1 }
          ]}
        >
          <Grid>
            <Col>
              <Field name={'latitude'} component={HiddenComponent} />
            </Col>
            <Col>
              <Field name={'longitude'} component={HiddenComponent} />
            </Col>
          </Grid>
          <Field
            value='hejsan'
            component={TextInput}
            label='Street name'
            name={'streetName'}
          />
          <Field
            name={'streetNumber'}
            component={TextInput}
            label='Street number'
            validate={[required, number]}
          />
          <Field name={'subLocality'} component={TextInput} label='District' />
          <Grid>
            <Col>
              <Field
                name={'locality'}
                component={TextInput}
                label='City'
                validate={[required]}
              />
            </Col>
            <Col>
              <Field
                name={'country'}
                component={TextInput}
                label='Country'
                validate={[required]}
              />
            </Col>
          </Grid>
        </View>
      </Content>
    )
  }
}
