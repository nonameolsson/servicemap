import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    alignContent: 'center',
    marginTop: 56
  },
  image: {
    alignSelf: 'center'
  },
  text: {
    textAlign: 'center',
    fontSize: 18,
    marginTop: 32
  }
})
