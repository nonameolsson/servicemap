import React from 'react'
import { View } from 'react-native'
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'

import PeopleList from '../Components/PeopleList'
import Fab from '../Components/FloatingActionButton'

// Styles
import styles from './Styles/PeopleScreenStyle'
import { Colors } from '../Themes'

class PeopleScreen extends React.Component {
  onPress () {
    const { dispatch } = this.props.navigation
    const NavigationAction = NavigationActions.navigate({
      routeName: 'AddPerson',
      params: { subtitle: 'Details', region: this.props.region }
    })
    dispatch(NavigationAction)
  }

  render () {
    return (
      <View style={styles.container}>
        <PeopleList
          data={this.props.persons}
          navigation={this.props.navigation}
        />

        <Fab
          backgroundColor={Colors.secondaryDark}
          icon='add'
          iconColor={Colors.white}
          onPress={() => this.onPress()}
        />
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    region: state.location.region,
    persons: state.persons.persons
  }
}

const mapDispatchToProps = dispatch => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(PeopleScreen)
