import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes'

export default StyleSheet.create({
  list: {
    backgroundColor: Colors.white,
    flex: 1
  }
})
