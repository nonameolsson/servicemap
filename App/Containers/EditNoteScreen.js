import React from 'react'
import {
  Container,
  Content,
  Item,
  Input,
  Label
} from 'native-base'
import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'
import I18n from 'react-native-i18n'
import Snackbar from 'react-native-snackbar'

// Custom components
import { HeaderButton } from '../Components/Navigation'
import EditNoteForm from '../Components/Forms/EditNoteForm'

// Actions
import NotesActions from '../Redux/NotesRedux'

// Styles
import styles from './Styles/EditNoteScreenStyle'

class EditNoteScreen extends React.Component {
  static navigationOptions = ({ navigation, screenProps }) => {
    const { params = {} } = navigation.state
    return {
      title: 'Edit note',
      headerRight: (
        <HeaderButton onPress={() => params.handleSave()} icon='checkmark' />
      )
    }
  }

  constructor (props) {
    super(props)

    const { date, text } = props.navigation.state.params.note

    this.state = {
      date,
      text
    }
  }

  componentDidMount = () => {
    // Bind component function `handleRemove` to header icon
    this.props.navigation.setParams({
      handleSave: () => this.saveChanges()
    })
  }

  /**
   * Update note with changes
   *
   */
  saveChanges = () => {
    const { id } = this.props.navigation.state.params.note
    const { date, text } = this.props.form.values
    const { key } = this.props.navigation.state.params // Get key to screen to go back to when saving
    const { dispatch } = this.props.navigation

    // Updated note object
    const note = {
      id,
      date,
      text
    }

    // Define actions
    // TODO: Use a reset with params if possible
    const backAction = NavigationActions.back({ key })

    // Dispatch actions
    this.props.editNote(note)

    dispatch(backAction)
    Snackbar.show({ title: 'Note updated ' })
  }

  onChangeText = text => {
    this.setState({ text })
  }

  render () {
    return (
      <Container>
        <Content padder>
          <EditNoteForm
            date={this.state.date}
            text={this.state.text}
            onSubmit={this.saveChanges}
          />
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    form: state.form.editNote
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    editNote: data => dispatch(NotesActions.editNote(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditNoteScreen)
