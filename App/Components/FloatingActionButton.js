// @flow
import React from 'react'
import PropTypes from 'prop-types'
import { Fab, Icon } from 'native-base'

// Types
type Props = {
  onPress: Function,
  icon?: string,
  backgroundColor?: string,
  iconColor?: string
}

const FloatingActionButton = ({
  backgroundColor,
  iconColor,
  icon,
  onPress
}: Props) => {
  return (
    <Fab
      style={{ backgroundColor }}
      position='bottomRight'
      onPress={onPress}
    >
      <Icon
        color={iconColor}
        name={icon}
      />
    </Fab>
  )
}

export default FloatingActionButton

Fab.defaultProps = {
  backgroundColor: '#e8dee3',
  icon: 'add',
  iconColor: '#000'
}

// Prop type warnings
Fab.propTypes = {
  backgroundColor: PropTypes.string,
  icon: PropTypes.string,
  iconColor: PropTypes.string,
  onPress: PropTypes.func.isRequired
}
