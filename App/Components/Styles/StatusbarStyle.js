import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes/'

export default StyleSheet.create({
  statusbar: {
    height: 24,
    backgroundColor: Colors.primary
  }
})
