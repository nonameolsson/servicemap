// @flow
import React from 'react'
import PropTypes from 'prop-types'
import { View } from 'native-base'
import { connect } from 'react-redux'
import { reduxForm, formValueSelector } from 'redux-form'
import Swiper from 'react-native-swiper'

// Custom components
import { Page1, Page2, Page3, Page4 } from './'

// Libs
import GetAddress from '../../../Lib/GetAddress'

// Types
import type { Region } from 'Map'
import type { Result } from 'Form'
type Props = {
  region: Region,
  formResult: Result,
  change: Function,
  onIndexChanged: Function,
  index: number
}

type State = {
  previewAddress: string,
  streetName: string
}

class NewPersonForm extends React.Component<Props, State> {
  static propTypes = {
    region: PropTypes.object.isRequired,
    formResult: PropTypes.object.isRequired,
    change: PropTypes.func.isRequired,
    onIndexChanged: PropTypes.func.isRequired,
    index: PropTypes.number.isRequired
  }

  constructor (props) {
    super(props)

    this.state = {
      previewAddress: 'Unkown',
      address: {
        streetName: '',
        streetNumber: '',
        district: '',
        city: '',
        country: ''
      }
    }
  }

  componentDidMount () {
    this.handleInitialize()
  }

  componentWillReceiveProps = nextProps => {
    if (nextProps.region.longitude !== this.props.region.longitude) {
      this.props.change('longitude', nextProps.region.longitude)
      this.getAddress()
    }
    if (nextProps.region.latitude !== this.props.region.latitude) {
      this.props.change('latitude', nextProps.region.latitude)
      this.getAddress()
    }

    // Navigate between screen
    if (nextProps.index > this.props.index) {
      this.refs.swiper.scrollBy(1)
    } else if (nextProps.index < this.props.index) {
      this.refs.swiper.scrollBy(-1)
    }
  }

  getAddress = async () => {
    const { formResult, region } = this.props

    const latitude = formResult.latitude !== undefined ? formResult.latitude : region.latitude
    const longitude = formResult.longitude !== undefined ? formResult.longitude : region.longitude

    const coords = {
      latitude,
      longitude
    }

    if ((typeof (coords.latitude) === 'number' && typeof (coords.longitude) === 'number')) {
      GetAddress(coords)
        .then(address => {
          const { streetName, streetNumber, district, locality, country } = address
          this.setState({
            address: {
              streetName: streetName,
              streetNumber: streetNumber,
              district: district,
              city: locality,
              country: country
            }
          })

          // Update the preview address
          this.updatePreviewAddress(address.formattedAddress)

          // Update form values
          this.props.change('streetName', streetName)
          this.props.change('streetNumber', streetNumber)
          this.props.change('district', district)
          this.props.change('city', locality)
          this.props.change('country', country)

          return address.formattedAddress
        })
    }
  }

  /**
   * Default data in form
   */
  handleInitialize () {
    const initData = {
      gender: 'male',
      longitude: this.props.region.longitude,
      latitude: this.props.region.latitude
    }

    this.props.initialize(initData)
    this.getAddress()
  }

  moveMarker = e => {
    const { latitude, longitude } = e.nativeEvent.coordinate
    this.props.change('latitude', latitude)
    this.props.change('longitude', longitude)

    this.getAddress()
  }

  updatePreviewAddress = previewAddress => {
    this.setState({
      previewAddress
    })
  }

  render () {
    const {
      change,
      formResult,
      initialRegion,
      navigation,
      onIndexChanged,
      region
    } = this.props

    return (
      <View style={{ flex: 1 }}>
        <Swiper
          onIndexChanged={index => onIndexChanged(index)}
          ref='swiper'
          loop={false}
          showsButtons={false}
          scrollEnabled={false}
          showsHorizontalScrollIndicator
          scrollsToTop
          showsPagination
        >
          <Page1 navigation={navigation} />
          <Page2
            change={change}
            getAddress={this.getAddress}
            initialRegion={initialRegion}
            moveMarker={e => this.moveMarker(e)}
            navigation={navigation}
            previewAddress={this.state.previewAddress}
            region={region}
          />
          <Page3
            address={this.state.address}
            navigation={navigation}
            formResult={formResult}
            change={change}
          />
          <Page4 navigation={navigation} formResult={formResult} />
        </Swiper>
      </View>
    )
  }
}

let FormComponent = NewPersonForm

// Decorate with redux-form
FormComponent = reduxForm({
  form: 'addNewPerson'
})(NewPersonForm)

// Decorate with connect to read form values
const selector = formValueSelector('addNewPerson') // <-- same as form name
FormComponent = connect(
  (state, props) => {
    // can select values individually
    const {
      name,
      age,
      language,
      comments,
      latitude,
      longitude,
      streetName,
      streetNumber,
      subLocality,
      locality,
      country
    } = selector(
      state,
      'name',
      'age',
      'language',
      'comments',
      'latitude',
      'longitude',
      'streetName',
      'streetNumber',
      'subLocality',
      'locality',
      'country'
    )
    return {
      formResult: {
        name,
        age,
        language,
        comments,
        latitude,
        longitude,
        streetName,
        streetNumber,
        subLocality,
        locality,
        country
      }
    }
  }
)(FormComponent)

export default FormComponent
