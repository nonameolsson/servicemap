import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    margin: 0,
    padding: 0
  }
})
